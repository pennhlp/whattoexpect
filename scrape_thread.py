"""
Scrape the threads in the 'March 2018 babies' forum
"""


import re
import time
from configparser import ConfigParser
from datetime import datetime
from multiprocessing import Process, Queue
from urlparse import urlparse, urlunparse

from pymongo import MongoClient
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException,\
    NoSuchWindowException, TimeoutException

import utility


WEB_OPTIONS = webdriver.ChromeOptions()
WEB_OPTIONS.add_argument('--incognito')
NUM_WORKERS = 4


CONFIG = ConfigParser()
CONFIG.read('config.cfg')


def compose_url(url_thread, page_num):
    res = urlparse(url_thread)
    segments = (
        res.scheme,
        res.netloc,
        res.path,
        res.params,
        'page={}'.format(page_num),
        res.fragment)
    return urlunparse(segments)


def extract_comment(comment):
    xpath_author = './/span[@class="creator-info"]'
    author = comment.find_element_by_xpath(xpath_author).text
    xpath_time = './/span[@class="whenwhere"]'
    created_at = comment.find_element_by_xpath(xpath_time).text
    xpath_text = './/span[@class="message-text"]'
    text = comment.find_element_by_xpath(xpath_text).text
    xpath_reply = './/a[@class="reply-to"]'
    reply_to = list()
    for rpl in comment.find_elements_by_xpath(xpath_reply):
        reply_to.append(rpl.text)
    return {
        'author': author,
        'author_id': comment.get_attribute('data-author-id'),
        'created_at': created_at,
        'message_id': comment.get_attribute('data-message-id'),
        'reply_to': '; '.join(reply_to),
        'text': text
    }


class TooManyPage(Exception):
    pass


class ThreadScraper:
    def __init__(self, worker_id, forum_name):
        self.worker_id = worker_id
        self.browser = webdriver.Chrome(options=WEB_OPTIONS)
        self.browser.set_page_load_timeout(20)
        self.forum_name = forum_name
        self.group = forum_name.replace('-', '_')

        self.client = MongoClient(CONFIG['hlp-db']['uri_windows'])
        self.db = self.client['what_to_expect']
        self.col_threads = self.db['threads_' + self.group]
        self.col_posts = self.db['posts_' + self.group]

    def turn_page(self):
        xpath_next_page = '//a[@class="page-link next"]'
        page_button = self.browser.find_element_by_xpath(xpath_next_page)
        _, self.browser = utility.get_url(page_button.get_attribute('href'), self.browser)

    def fetch_main_post(self):
        xpath_main_post = '//div[@class="message-panel-container" and .//div[@class="topic-message"]]'
        main_post = self.browser.find_element_by_xpath(xpath_main_post)
        xpath_author = './/span[@class="author-info"]'
        author = main_post.find_element_by_xpath(xpath_author).text
        try:
            author = re.search(r'^(.*)\s*wrote:', author).group(1)
        except TypeError, e:
            print e
            print author
            author = 'TypeError: ' + str(e)
        xpath_time = './/span[@class="whenwhere"]'
        created_at = main_post.find_element_by_xpath(xpath_time).text
        xpath_text = './/div[@class="topic-message"]'
        text = main_post.find_element_by_xpath(xpath_text).text
        return {
            'author': author,
            'author_id': main_post.get_attribute('data-author-id'),
            'created_at': created_at,
            'is_main': True,
            'message_id': main_post.get_attribute('data-message-id'),
            'text': text
        }

    def fetch_comments(self, url_thread):
        xpath_comments = ('.//div[@class="message-panel-container" '
                          'and @data-message-id and .//span[@class="creator-info"]]')
        comments = self.browser.find_elements_by_xpath(xpath_comments)
        utility.pause_between_actions()
        if not comments:
            print '\tWorker', self.worker_id, ':\tNo comment in this thread'
            return comments
        comments = [extract_comment(cmt) for cmt in comments]
        print '\tWorker', self.worker_id, ':\t{} comments on page 1'.format(len(comments))

        xpath_page = '//a[@class="page-link"]'
        page_buttons = self.browser.find_elements_by_xpath(xpath_page)
        max_page = max(int(btn.text) for btn in page_buttons) if page_buttons else 1
        print '\tWorker', self.worker_id, ':\t{} pages in total'.format(max_page)
        if max_page > 300:
            print '\tWorker', self.worker_id, ': Too many pages,', max_page
            raise TooManyPage

        page_num = 1
        while page_num < max_page:
            page_num += 1
            try:
                self.turn_page()
                latest_comments = self.browser.find_elements_by_xpath(xpath_comments)
                if not latest_comments:
                    print '\tWorker', self.worker_id, ':\tNo comment found, will wait 15 sec and retry'
                    time.sleep(10)
                    url = compose_url(url_thread, page_num)
                    is_loaded, self.browser = utility.get_url(url, self.browser)
                    if not is_loaded:
                        print '\tWorker', self.worker_id, ':\tCannot re-load page'
                        break
                    latest_comments = self.browser.find_elements_by_xpath(xpath_comments)
                comments += [extract_comment(cmt) for cmt in latest_comments]
                print '\tWorker', self.worker_id, ':\t{} comments on page {}'.format(len(latest_comments), page_num)
                utility.pause_between_actions()
            except NoSuchElementException:
                print 'Worker', self.worker_id, ':\tNo next page button'
                break
        print '\tWorker', self.worker_id, ':\t{} comments loaded in total'.format(len(comments))
        return comments

    def scrape_thread(self, thread):
        thread_info = {field: thread[field] for field in ['_id', 'topic_id']}
        # print '\n', thread['title'], '\t', thread['url']
        is_loaded, self.browser = utility.get_url(thread['url'], self.browser)
        if not is_loaded:
            print 'Worker', self.worker_id, ':\tCannot load thread, will wait 15 sec and retry'
            time.sleep(15)
            is_loaded, self.browser = utility.get_url(thread['url'], self.browser)
            if not is_loaded:
                return False
        try:
            main_post = self.fetch_main_post()
            main_post['thread_info'] = thread_info
            if not self.col_posts.find_one({'message_id': main_post['message_id']}):
                self.col_posts.insert_one(main_post)
                print '\tWorker', self.worker_id, ':\tmain post inserted'
            comments = [
                cmt for cmt in self.fetch_comments(thread['url'])
                if not self.col_posts.find_one({'message_id': cmt['message_id']})]
        except (NoSuchElementException, NoSuchWindowException, AttributeError, TooManyPage), e:
            print 'Worker', self.worker_id, e
            return False

        print '\tWorker', self.worker_id, '\t', len(comments), 'comments to insert'
        if comments:
            for cmt in comments:
                cmt['thread_info'] = thread_info
            self.col_posts.insert_many(comments)
        return True


def run_worker(worker_id, forum_name, queue):
    time.sleep(3)
    worker = ThreadScraper(worker_id, forum_name)
    while True:
        thread = queue.get(True, None)
        if not thread:
            print 'Worker', worker_id, ':\tNo more task to claim'
            worker.browser.quit()
            break
        idx, thread = thread
        print 'Worker', worker_id, '\t', idx, thread['url']
        try:
            scraped = worker.scrape_thread(thread)
        except TimeoutException, e:
            print 'Worker', worker_id, e
            worker.browser = utility.restart_browser(worker.browser)
            continue
        if scraped:
            worker.col_threads.update_one(
                {
                    '_id': thread['_id']
                },
                {
                    '$set': {
                        'scraped': True
                    }
                }
            )


def scrape_all_threads(forum_name):
    group = forum_name.replace('-', '_')
    client = MongoClient(CONFIG['hlp-db']['uri_windows'])
    db = client['what_to_expect']
    col_threads = db['threads_' + group]
    col_posts = db['posts_' + group]
    name_index = 'message_id'
    if not col_posts.count() or name_index not in col_posts.index_information():
        col_posts.create_index('message_id', name=name_index)
    to_scrape = list(col_threads.find({'scraped': {'$ne': True}}))
    queue = Queue(NUM_WORKERS * 3)

    processes = list()
    for worker_id in xrange(NUM_WORKERS):
        proc = Process(target=run_worker, args=(worker_id, forum_name, queue))
        proc.daemon = True
        proc.start()
        processes.append(proc)

    for idx, thread in enumerate(to_scrape):
        if idx % NUM_WORKERS == 0:
            for worker_id, proc in list(enumerate(processes)):
                if proc.is_alive():
                    continue
                processes[worker_id] = Process(target=run_worker, args=(worker_id, forum_name, queue))
                processes[worker_id].daemon = True
                processes[worker_id].start()
                with open('thread_scraper.log', 'ab') as f_log:
                    msg = str(datetime.now()) + '\t--\tWorker {} restarted'.format(worker_id)
                    print msg
                    f_log.write(msg)
        queue.put((idx, thread))

    for _ in xrange(NUM_WORKERS):
        queue.put(None)

    for proc in processes:
        proc.join()


if __name__ == '__main__':
    # for month_group in [
    #     'july',
    #     'august',
    #     'october',
    #     'november',
    #     'december'
    # ]:
    #     scrape_all_threads(month_group + '-2018-babies')

    for month in [
        'january', 'february', 'march', 'april', 'may', 'june',
        # 'july', 'august', 'september', 'october', 'november', 'december'
    ]:
        scrape_all_threads(month + '-2019-babies')
