"""
Remove the posts with invalid timestamps
"""


import csv
import os

from dateutil.parser import parse
from pymongo import MongoClient


MONGO_CLIENT = MongoClient('localhost', 27017)
DB = MONGO_CLIENT['what_to_expect']
DIR_INVALID = 'invalid_posts'


def detect_invalid_ts(group):
    """
    As is named
    :param group: group name
    :return: None
    """
    col_threads = DB['threads_' + group]
    col_posts = DB['posts_' + group]
    invalid_posts, ids_thread = list(), set()
    print 'Detecting posts with invalid timestamps from', group

    for post in col_posts.find():
        try:
            parse(post['created_at'])
        except ValueError:
            invalid_posts.append(post)
            ids_thread.add(post['thread_info']['_id'])

    return invalid_posts, list(col_threads.find({'_id': {'$in': list(ids_thread)}}))


def clean_db(group, run_clean=False):
    group = group.replace('-', '_')
    invalid_posts, invalid_threads = detect_invalid_ts(group)
    for trd in invalid_threads:
        print trd['title'].encode('utf-8')
    if not invalid_posts:
        print 'No post with invalid timestamps'
        return

    # Write invalid posts to file
    path_out = os.path.join(DIR_INVALID, group + '.tsv')
    with open(path_out, 'wb') as f_out:
        writer = csv.writer(f_out, delimiter='\t')
        for post in invalid_posts:
            row = [
                post['_id'],
                post['thread_info']['_id'],
                post['author'].encode('utf-8'),
                post['created_at'],
                post['text'].encode('utf-8')
            ]
            writer.writerow(row)

    if not run_clean:
        return
    print 'Removing invalid posts and threads...'
    col_posts = DB['posts_' + group]
    col_posts.delete_many({
        '_id': {
            '$in': [doc['_id'] for doc in invalid_posts]
        }
    })

    col_threads = DB['threads_' + group]
    col_threads.update_many(
        {
            '_id': {
                '$in': [doc['_id'] for doc in invalid_threads]
            }
        },
        {
            '$unset': {
                'scraped': '',
                'main_post': '',
                'comments': ''
            }
        }
    )


if __name__ == '__main__':
    if not os.path.isdir(DIR_INVALID):
        os.mkdir(DIR_INVALID)

    for month in [
            # 'july',
            'august',
            'october',
            'november',
            'december'
    ]:
        clean_db(month + '-2018-babies', run_clean=False)
        print '\n'

    for month in [
            'january', 'february', 'march', 'april', 'may', 'june',
            'july', 'august', 'september', 'october', 'november', 'december'
    ]:
        clean_db(month + '-2017-babies', run_clean=False)
        print '\n'




