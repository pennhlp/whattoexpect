# Web Scraping for whattoexpect.com

The bot launches instances of Chrome web browser and fetch threads from the website of whattoexpect.com.

## Requirements

* Python 2.7
* pymongo == 3.8.0
* selenium == 3.141.0
* The latest [Selenium Chrome driver](https://chromedriver.chromium.org/downloads) should be placed in the root directory together with the code.

## Descriptions

* `assemble_threads.py` fetches the metadata of threads from target month groups.
* `descriptive_statistics.py` generates descriptive statistics (e.g., threads, posts, users).
* `export_threads.py` exports scraped threads.
* `remove_invalid_timestamp.py` removes posts with invalid timestamps.
* `scrape_thread.py` scrapes threads the metadata of which are assembled using `assemble_threads.py`.
* `utility.py` contains utility functions.

## Tasks

Before performing the tasks, remember to properly configure the creation of `MongoClient` instances (and maybe also names of the database storing the threads and posts). In addition, the target month groups to scrape are set in the `__main__` code block.

The tasks are documented below in the order of deployment.

### Assembling threads

code: `assemble_threads.py`

### Scraping threads

code: `scrape_thread.py`

The task involves `multiprocessing`. The number of worker processes is defined as the variable `NUM_WORKERS`.

### Exporting threads

code: `export_threads.py`

For each month group, there is one MongoDB collection storing the metadata of threads and another collection for the posts (i.e., initial posts of threads and comments). Therefore, before the first time of exporting the threads of a month group, `ThreadExporter.assemble_posts` needs to run to assemble the MongoDB object ids of the posts for each thread. Afterwards, `ThreadExporter.export_all_threads` exports each thread into a separate file.


