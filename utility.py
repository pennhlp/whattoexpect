"""
Utility functions
"""


import random
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.common.keys import Keys

WEB_OPTIONS = webdriver.ChromeOptions()
WEB_OPTIONS.add_argument('--incognito')


def pause_between_actions(len_base=1, len_var=1):
    len_sleep = len_base + len_var * random.random()
    time.sleep(len_sleep)


def restart_browser(browser):
    browser.quit()
    browser = webdriver.Chrome(options=WEB_OPTIONS)
    browser.set_page_load_timeout(15)
    return browser


def get_url(url, browser):
    is_loaded = True

    try:
        browser.get(url)
    except TimeoutException:
        try:
            webdriver.ActionChains(browser).send_keys(Keys.ESCAPE).perform()
        except TimeoutException:
            pause_between_actions(len_base=2)
            browser = restart_browser(browser)
            is_loaded = False
    except WebDriverException:
        pause_between_actions(len_base=2)
        browser = restart_browser(browser)
        is_loaded = False

    pause_between_actions()
    # btn = browser.find_element_by_xpath('button2')
    # if btn:
    #     btn.click()
    return is_loaded, browser

