"""
Export each thread to a separate file
"""


import csv
import os
import re
import shutil
import time
from collections import defaultdict
from dateutil.parser import parse
from pprint import pprint

from pymongo import MongoClient


CLIENT = MongoClient('localhost', 27017)
DB = CLIENT['what_to_expect']


def format_filename(topic_id, num_tokens=5):
    """
    Generate filename from topic_id
    :param topic_id:
    :param num_tokens:
    :return:
    """
    tokens = [tk for tk in topic_id.split('-') if tk]
    if len(tokens) > num_tokens + 1:
        suffix = tokens[-1]
        tokens = tokens[:num_tokens]
        tokens.append(suffix)
    return '-'.join(tokens)


def format_row(post):
    """
    Convert a post into a row for the output tsv file
    :param post: a main post or a comment
    :return: a list of values ready for writing to tsv file
    """
    reply_to = ''
    if 'reply_to' in post and post['reply_to']:
        reply_to = post['reply_to'].encode('utf-8') + ' '

    text = reply_to + re.sub(r'\s', ' ', post['text']).encode('utf-8')

    return [
        post['author'].encode('utf-8'),
        post['author_id'],
        post['created_at'],
        text,
        post['message_id']
    ]


class ThreadExporter(object):
    """
    Assemble and export the posts of each thread
    """
    def __init__(self, group):
        self.group = group.replace('-', '_')
        self.col_threads = DB['threads_' + self.group]
        self.col_posts = DB['posts_' + self.group]
        self.dir_output = os.path.join(
            os.path.expanduser('~'), 'Documents', 'what_to_expect_data', self.group)

    def assemble_posts(self):
        """
        Add the object id of each post into the thread
        :return: None
        """
        print 'Assembling the posts of each thread from {}...'.format(self.group)
        for post in self.col_posts.find():
            update = {
                '$set': {
                    'main_post': post['_id']
                }
            } if 'is_main' in post and post['is_main'] else {
                '$addToSet': {
                    'comments': post['_id']
                }
            }

            self.col_threads.update_one(
                {
                    '_id': post['thread_info']['_id']
                },
                update
            )

    def export_thread(self, thread, filenames_used):
        """
        Export the posts of thread
        :param thread: the thread doc in COL_THREADS
        :param filenames_used: dict mapping filename to number of used times
        :return: None
        """
        # Retrieve main post and comments
        if 'main_post' not in thread:
            print 'Main post missing:\t', thread['topic_id']
            return

        main_post = self.col_posts.find_one({'_id': thread['main_post']})
        comments = list()
        if 'comments' in thread:
            flt = {
                '_id': {
                    '$in': thread['comments']
                }
            }
            comments = list()
            for cmt in self.col_posts.find(flt):
                if cmt['created_at'].strip():
                    comments.append(cmt)
                else:
                    pprint(cmt)
            comments.sort(key=lambda x: parse(x['created_at']))

        # Write to file
        filekey = filename = format_filename(thread['topic_id'])
        if filekey in filenames_used:
            print '\nDuplicated filename:\t', filekey
            filename += '-' + str(filenames_used[filekey] + 1)
            print 'will use name:\t', filename, '\n'
        filenames_used[filekey] += 1
        filepath = os.path.join(self.dir_output, filename + '.tsv')
        with open(filepath, 'wb') as f_output:
            writer = csv.writer(f_output, delimiter='\t')
            writer.writerow([
                re.sub(r'\s', ' ', thread['title']).encode('utf-8')])
            writer.writerow(format_row(main_post))
            for cmt in comments:
                writer.writerow(format_row(cmt))

    def export_all_threads(self):
        """
        Export all threads for the group
        """
        if os.path.isdir(self.dir_output):
            shutil.rmtree(self.dir_output)
            time.sleep(5)
        os.mkdir(self.dir_output)

        print 'Exporting the threads of {}...'.format(self.group)
        threads = list(self.col_threads.find({'scraped': True}))
        filenames_used = defaultdict(int)
        for idx, trd in enumerate(threads):
            if idx % 1000 == 0:
                print '{} / {}'.format(idx, len(threads))

            try:
                self.export_thread(trd, filenames_used)
            except IOError:
                print 'IOError:\t', trd['topic_id']


def main():
    """
    The main func
    """
    # for month in [
    #         'july',
    #         'august',
    #         'october',
    #         'november',
    #         'december'
    # ]:
    #     exporter = ThreadExporter(month + '-2018-babies')
    #     exporter.assemble_posts()
    #     exporter.export_all_threads()

    for month in [
            'january', 'february', 'march', 'april', 'may', 'june',
            'july', 'august', 'september', 'october', 'november', 'december'
    ]:
        exporter = ThreadExporter(month + '-2016-babies')
        exporter.assemble_posts()
        exporter.export_all_threads()


if __name__ == '__main__':
    main()
