"""
Assemble the threads of birth month forum at whattoexpect.com
"""


import re
from configparser import ConfigParser
from urlparse import urlunparse

from pymongo import MongoClient
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchWindowException

import utility


WEB_OPTIONS = webdriver.ChromeOptions()
WEB_OPTIONS.add_argument('--incognito')
prefs = {
    'profile.managed_default_content_settings.images': 2,
    'disk-cache-size': 4096
}
WEB_OPTIONS.add_experimental_option('prefs', prefs)
XPATH_NEXT_PAGE = '//a[@class="page-link next"]'
XPATH_THREADS = '//div[@class="topic-panel-container"]'


def extract_thread(thread):
    xpath_topic_heading = './/a[@class="topic-heading"]'
    heading = thread.find_element_by_xpath(xpath_topic_heading)
    xpath_creator_info = './/span[@class="creator-info"]'
    creator_info = thread.find_element_by_xpath(xpath_creator_info)
    if not creator_info:
        return None
    msg = type(creator_info.text)
    assert isinstance(creator_info.text, str) or isinstance(creator_info.text, unicode), msg
    m = re.search(r'created\sby\s(.+)$', creator_info.text, flags=re.I)
    return {
        'title': heading.text,
        'topic_id': thread.get_attribute('data-topic-id'),
        'url': heading.get_attribute('href'),
        'creator': m.group(1) if m else ''
    }


class ThreadAssembler:
    def __init__(self, forum_name):
        self.browser = webdriver.Chrome(options=WEB_OPTIONS)
        self.browser.set_page_load_timeout(15)
        self.forum_name = forum_name
        self.group = forum_name.replace('-', '_')
        self.url_page = urlunparse((
            'https',
            'community.whattoexpect.com',
            '/forums/{}.html'.format(forum_name),
            '',
            'page={}',
            ''))
        self.max_page = None

        config = ConfigParser()
        config.read('config.cfg')
        self.client = MongoClient(config['hlp-db']['uri_windows'])
        self.db = self.client['what_to_expect']
        self.col_threads = self.db['threads_' + self.group]
        self.col_progress = self.db['progress_threads']

    def init_collections(self):
        name_index = 'topic_id'
        if self.col_threads.count() == 0 or name_index not in self.col_threads.index_information():
            self.col_threads.create_index('topic_id', name=name_index)

    def retrieve_progress(self):
        flt = {'forum': self.group}
        doc_progress = self.col_progress.find_one(flt)
        if doc_progress and 'last_page' in doc_progress:
            return doc_progress['last_page']
        if not doc_progress:
            self.col_progress.insert_one(flt)
        return 0

    def next_page(self, last_page):
        if last_page == 0:
            return 'https://community.whattoexpect.com/forums/{}.html'.format(self.forum_name)
        # else:
        #     try:
        #         self.browser.find_element_by_xpath(XPATH_NEXT_PAGE)
        #     except NoSuchElementException:
        #         return None
        return self.url_page.format(last_page + 1)

    def get_max_page(self):
        xpath_page_link = '//a[@class="page-link"]'
        page_links = [
            elem.get_attribute('href') for elem in self.browser.find_elements_by_xpath(xpath_page_link)]
        return max(
            int(re.search(r'page=(\d+)$', link).group(1))
            for link in page_links if re.search(r'page=(\d+)$', link))

    def assemble_threads(self):
        """
        The main function
        :return: None
        """
        last_page = self.retrieve_progress()
        while self.max_page is None or last_page < self.max_page:
            url = self.next_page(last_page)
            last_page += 1
            print last_page, '/', self.max_page
            is_loaded, self.browser = utility.get_url(url, self.browser)
            if not is_loaded:
                print 'Page could not be loaded:\t', url
            else:
                if self.max_page is None:
                    self.max_page = self.get_max_page()
                try:
                    threads = [
                        extract_thread(trd) for trd in self.browser.find_elements_by_xpath(XPATH_THREADS)]
                    threads = [
                        trd for trd in threads
                        if trd and not self.col_threads.find_one({
                            'topic_id': trd['topic_id']
                        })
                    ]
                    print '\t', len(threads), 'threads fetched'
                    if threads:
                        self.col_threads.insert_many(threads)
                    print '\t', len(threads), 'threads inserted'
                except TimeoutException:
                    print 'Timeout when extracting threads'
                except NoSuchWindowException:
                    print 'No such window'
            self.col_progress.update_one(
                {
                    'forum': self.group
                },
                {
                    '$set': {
                        'last_page': last_page
                    }
                }
            )
        self.browser.quit()


if __name__ == '__main__':
    for month in [
            'january', 'february', 'march', 'april', 'may', 'june',
            # 'july', 'august', 'september', 'october', 'november', 'december'
    ]:
        scraper = ThreadAssembler('{}-2019-babies'.format(month))
        scraper.assemble_threads()

