"""
Get the descriptive statistics for the scraped birth month groups
"""


from pprint import pprint

import pandas as pd
from pandas import DataFrame
from pymongo import MongoClient

from typing import Dict, List


MONGO_CLIENT = MongoClient('localhost', 27017)
DB = MONGO_CLIENT['what_to_expect']


def count_month_group(group_name):
    # type: (str) -> Dict[str, int]
    thread_col_name = 'threads_' + group_name.replace('-', '_')
    thread_col = DB[thread_col_name]
    post_col_name = 'posts_' + group_name.replace('-', '_')
    post_col = DB[post_col_name]

    cnt = {
        'month_group': group_name,
        'threads':  thread_col.count(),
        'posts': post_col.count(),
        'initial_posts': post_col.count({'is_main': True})
    }

    ids_user, ids_user_initial = set(), set()
    for post in post_col.find():
        author_id = post['author_id']
        ids_user.add(author_id)
        if 'is_main' in post and post['is_main']:
            ids_user_initial.add(author_id)

    cnt['users'] = len(ids_user)
    cnt['users_initiating_threads'] = len(ids_user_initial)
    pprint(cnt)
    return cnt


def get_counts(months_to_count):
    # type: (List[str]) -> None
    counts = list()
    for group_name in months_to_count:
        counts.append(count_month_group(group_name))
    df = pd.DataFrame(
        counts,
        columns=[
            'month_group', 'threads', 'posts', 'initial_posts', 'users', 'users_initiating_threads']
    )  # type: DataFrame
    df.to_csv(
        'WhatToExpect-descriptive_statistics.tsv',
        sep='\t',
        index=False)


if __name__ == '__main__':

    get_counts([
        'january-2018-babies',
        'february-2018-babies',
        'march-2018-babies',
        'april-2018-babies',
        'may-2018-babies',
        'june-2018-babies',
        'september-2018-babies'
    ])


